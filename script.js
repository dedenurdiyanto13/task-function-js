alert(`
==========> SELAMAT DATANG DI GAME TEBAK ANGKA

Aturan main :
- Masukkan hanya angka 1, 2 atau 3 saja sebagai jawaban
- Bermain selama 2 match
- Tebakan benar terbanyak adalah pemenangnya

==========> SELAMAT BERMAIN
`);

let point_g_1 = 0;
let point_g_2 = 0;
let match = 1;
let stop = true;

while (stop) {
  let gamer_1 = parseInt(prompt("Silahkan masukan angka (Gamer 1)"));
  let gamer_2 = parseInt(prompt("Silahkan masukan angka (Gamer 2)"));

  let random = getRandom();
  let ulangi = validasi(gamer_1, gamer_2);
  if (!ulangi) {
    stop = window.confirm("Ulangi permainan?");
  } else {
    if (gamer_1 !== random && gamer_2 !== random) {
      alert("Hasil SERI");
    } else {
      if (gamer_1 === random) {
        alert("Gamer 1 Menang");
        point_g_1++;
      }
      if (gamer_2 === random) {
        alert("Gamer 2 Menang");
        point_g_2++;
      }
    }

    alert(`
    -----------------------------------
    Nilai yang dicari: ${random}
    -----------------------------------
    Point Sementara:
    -----------------------------------
    - Gamer 1: ${point_g_1}
    - Gamer 2: ${point_g_2}
    -----------------------------------
    `);

    match++;
    if (match <= 2) {
      stop = window.confirm("match " + match + "?");
    } else {
      if (point_g_1 > point_g_2) {
        alert("Selamat Gamer 1 kamu pemenangnya");
        stop = false;
      } else if (point_g_1 < point_g_2) {
        alert("Selamat Gamer 2 kamu pemenangnya");
        stop = false;
      } else {
        alert(`Gamer 1 dan Gamer 2 sama kuatnya`);
        match = 1;
        point_g_1 = 0;
        point_g_2 = 0;
        stop = window.confirm("Apakah mau main lagi?");
      }
    }
  }
}

function validasi(gamer1, gamer2) {
  if (gamer1 === gamer2) {
    alert("angka tidak boleh sama");
    return false;
  }

  if (gamer1 < 1 || gamer2 < 1) {
    alert("angka tidak boleh lebih kecil dari 1");
    return false;
  }

  if (gamer1 > 3 || gamer2 > 3) {
    alert("angka tidak boleh lebih besar dari 3");
    return false;
  }

  if (isNaN(gamer1) || isNaN(gamer2)) {
    alert("salah satu gamer belum menebak");
    return false;
  }

  return true;
}

function getRandom() {
  const range = [1, 2, 3];
  let isNotRandom = true;
  while (isNotRandom) {
    let random = Math.floor(Math.random() * 10);
    let ketemu = range.find((r) => r === random);
    if (ketemu) {
      isNotRandom = false;
      return random;
    }
  }
}

console.log(getRandom());
